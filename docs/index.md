# Introduction

RepoSherlock is a package intended to facilitate retrieving information from software repositories hosted by services such as GitHub, GitLab, BitBucket, or similar services.
It uses their APIs to fetch issue, pull/merge request, and commit data for further processing.

## Who is this for?

I developed this primarily for researchers interested in studying software repositories, since it helps me with my work too.
While it was originally meant for research purposes, it is conceivable that it would have other applications as well.
As such, this project is licensed under the MIT license.

## What services does RepoSherlock support queries for?

So far, RepoSherlock supports data extraction from repositories hosted on GitHub.
I have plans to port my previous BitBucket client to this project too, and eventually, add a GitLab client as well.
Stay up-to-date by monitoring RepoSherlock's [issues page](https://gitlab.com/omazhary/reposherlock/-/issues).
