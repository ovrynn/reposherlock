# How Do I Use RepoSherlock?

RepoSherlock supports two modes of use:
- as a standalone application, and
- as a module within your python script.

## As a standalone application

Once installed, you can use RepoSherlock as a standalone application in your terminal of choice.
It supports the following arguments:

- **-h**, **--help**: Shows a help message and exits.
- **--user <username>**: Your username on the service from which you want to retrieve data.
- **--token <token>**: The token provided to you by your repository service.
- **--target <owner/repository>**: The repository whose data you want to pull.
- **--type [GitHub|BitBucket]**: Your repository management service. Currently, only GitHub is supported.
- **--outdir <path/to/output/directory>**: The output directory where RepoSherlock should save the queried data.
- **--pages <number_of_pages>**: The maximum number of pages of data to fetch. Default is 1000. Naturally, RepoSherlock will stop once no more data is available.

A typical command using RepoSherlock looks like the following example:

```shell
$ reposherlock --user omazhary \
--token <long_alphanumeric_token> \
--target omazhary/reposherlock \
--type GitHub \
--outdir output/.
```

## As a Python module

You can build a python script and use RepoSherlock within it to fetch data on the fly to do with as you please.
For instance, if you wanted to create a GitHub client to use in your python script, you would import it as a dependency, and give it the necessary information:

```python
from reposherlock.github import GitHub

client = GitHub('omazhary', 'my_long_alphanumeric_token')
issues = client.get_issues('omazhary/reposherlock', 1000)
pull_requests = client.get_pull_requests('omazhary/reposherlock', 1000)
commits = client.get_commits('omazhary/reposherlock', 1000)
```

Further documentation can be found [here](https://reposherlock.readthedocs.io/).

